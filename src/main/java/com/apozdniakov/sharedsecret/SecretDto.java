package com.apozdniakov.sharedsecret;

class SecretDto {
    private String secret;

    public SecretDto() {
    }

    public SecretDto(String secret) {
        this.secret = secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getSecret() {
        return secret;
    }
}
