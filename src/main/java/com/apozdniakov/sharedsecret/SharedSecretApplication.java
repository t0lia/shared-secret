package com.apozdniakov.sharedsecret;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SharedSecretApplication {

	public static void main(String[] args) {
		SpringApplication.run(SharedSecretApplication.class, args);
	}

}
