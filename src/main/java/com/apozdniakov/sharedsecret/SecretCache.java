package com.apozdniakov.sharedsecret;

import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class SecretCache {

    private final Map<UUID, String> secrets = new ConcurrentHashMap<>();

    public Optional<String> get(String key) {
        UUID uuid = UUID.fromString(key);

        if (!secrets.containsKey(uuid)) {
            return Optional.empty();
        }
        String result = secrets.get(uuid);
        secrets.remove(uuid);
        return Optional.of(result);
    }

    public String post(String secret) {
        UUID key = UUID.randomUUID();
        secrets.put(key, secret);
        return key.toString();
    }
}
