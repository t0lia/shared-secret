package com.apozdniakov.sharedsecret;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static java.util.Collections.singletonMap;


@RestController
@RequestMapping("secret")
public class ApplicationController {

    private final SecretCache cache;

    public static final String PREFIX = "http://localhost:8080/secret/get/";


    public ApplicationController(SecretCache cache) {
        this.cache = cache;
    }

    @PostMapping("post")
    public ResponseEntity approveAccount(@RequestBody SecretDto secretDto) {
        String uri = PREFIX + cache.post(secretDto.getSecret());
        return ResponseEntity.ok(singletonMap("link", uri));
    }

    @GetMapping("get/{key}")
    public ResponseEntity getBrands(@PathVariable("key") String guid) {
        return ResponseEntity.ok(cache.get(guid).orElse("secret is gone"));
    }
}
