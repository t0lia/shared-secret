package com.apozdniakov.sharedsecret;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Optional;

class SecretCacheTest {
    @Test
    void putToCache() {
        SecretCache secretCache = new SecretCache();
        String uri = secretCache.post("secret");

        Optional<String> s = secretCache.get(uri);
        Assertions.assertTrue(s.isPresent());
        Assertions.assertEquals(s.get(), "secret");

        Optional<String> s2 = secretCache.get(uri);
        Assertions.assertTrue(s2.isEmpty());
    }
}